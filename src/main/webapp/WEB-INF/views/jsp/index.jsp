<%--
  Created by IntelliJ IDEA.
  User: lonsomehell
  Date: 11/17/15
  Time: 3:26 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="<spring:url value="assets/css/bootstrap.min.css"></spring:url>">
    <link rel="stylesheet" href="<spring:url value="assets/css/bootstrap-theme.min.css"></spring:url>">
    <meta charset="UTF-8">
    <title>Welcome</title>
</head>
<body>
<div class="container">
    <h1>Welcome to our website.</h1>
</div>
</body>
</html>
