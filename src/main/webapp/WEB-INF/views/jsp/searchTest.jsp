<%--
  Created by IntelliJ IDEA.
  User: lonsomehell
  Date: 11/20/15
  Time: 11:41 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>

    <link rel="stylesheet" href="<spring:url value="assets/css/bootstrap.min.css"></spring:url>">
    <link rel="stylesheet" href="<spring:url value="assets/css/bootstrap-theme.min.css"></spring:url>">
    <title>SearchForm</title>
</head>
<body>
<div class="container">
    <h3><spring:message code="bookSearch"/>:</h3>
    <form:form method="GET" action="searchResult.html" modelAttribute="multiCriteriaForm" cssClass="form-group">
        <table>
            <tr>
                <td><label><spring:message code="book.ISBN"/>:</label></td>
                <td><form:input path="ISBN" cssClass="form-control"/></td>
            </tr>
            <tr>
                <td><label><spring:message code="book.name"/>:</label></td>
                <td><form:input path="bookName" cssClass="form-control"/></td>
            </tr>
            <tr>
                <td><label><spring:message code="author.name"/>:</label></td>
                <td><form:input path="authorName" cssClass="form-control"/></td>
            </tr>
            <tr>
                <td><label><spring:message code="category.name"/>:</label></td>
                <td><form:input path="categoryName" cssClass="form-control"/></td>
            </tr>
            <tr>
                <spring:message code="buttonSubmit" var="buttonSubmit"></spring:message>
                <td><input type="submit" value="${buttonSubmit}" class="btn-primary"/></td>
            </tr>
        </table>
    </form:form>
</div>
</body>
</html>
