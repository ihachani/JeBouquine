<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: lonsomehell
  Date: 11/21/15
  Time: 12:05 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title><spring:message code="bookList"/></title>
    <link rel="stylesheet" href="<spring:url value="assets/css/bootstrap.min.css"></spring:url>">
    <link rel="stylesheet" href="<spring:url value="assets/css/bootstrap-theme.min.css"></spring:url>">
</head>
<body>
<div class="container">
    <h1>BookList</h1>
    <table class="table table-hover">
        <thead>
        <tr>
            <th><spring:message code="book.ISBN"/></th>
            <th><spring:message code="book.name"/></th>
            <th><spring:message code="book.price"/></th>
            <th><spring:message code="book.description"/></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="book" items="${books}">
            <tr>
                <td>${book.ISBN}</td>
                <td>${book.name}</td>
                <td>${book.price}</td>
                <td>${book.description}</td>
            </tr>
        </c:forEach>
        </tbody>

    </table>
</div>
</body>
</html>
