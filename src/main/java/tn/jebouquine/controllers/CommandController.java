package tn.jebouquine.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tn.jebouquine.dao.AccountDAO;
import tn.jebouquine.pojo.Account;
import tn.jebouquine.pojo.Cart;
import tn.jebouquine.pojo.Command;
import tn.jebouquine.services.CartHandler;
import tn.jebouquine.services.CommandHandler;

import java.util.ArrayList;

@Controller
@RequestMapping("/command")
public class CommandController {

    @Autowired
    Cart cart;

    @Autowired
    AccountDAO accountDAO;
    @Autowired
    CommandHandler commandHandler;

    @Autowired
    CartHandler cartHandler;

    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String showCommands(@ModelAttribute("commands") ArrayList<Command> commands, Model model) {
        commands = new ArrayList<Command>(commandHandler.getUserCommands(getAccount()));
        model.addAttribute("commands", commands);
        return "commands";
    }

    @RequestMapping(value = "/passCommand", method = RequestMethod.POST)
    public String passCommand() {
        Account account = getAccount();
        generateCommand(account);
        return "redirect:/index.html";
    }

    //TODO relocate this to utilities.
    private Account getAccount() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return accountDAO.retrieve(auth.getName());
    }

    private void generateCommand(Account account) {

        Cart persisted = new Cart();
        persisted.setBooks(cart.getBooks());
        persisted.setTotal(cart.getTotal());
        persisted = cartHandler.saveCart(persisted);
        Command command = commandHandler.createCommand(account, persisted);
        commandHandler.saveCommand(command);
//        cart = new Cart();
    }
}
