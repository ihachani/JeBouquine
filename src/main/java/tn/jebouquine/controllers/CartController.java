package tn.jebouquine.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tn.jebouquine.pojo.Book;
import tn.jebouquine.pojo.Cart;
import tn.jebouquine.services.BookSearcher;
import tn.jebouquine.services.CartHandler;

import java.util.ArrayList;

@Controller
@RequestMapping("/cart")
public class CartController {
    @Autowired
    BookSearcher bookSearcher;

    @Autowired
    CartHandler cartHandler;

    @Autowired
    Cart cart;

    @RequestMapping(method = RequestMethod.GET)
    public String showBooksInCart(@ModelAttribute("booksInCart") ArrayList<Book> booksInCart, Model model) {
//        System.out.println(cart);
//        ArrayList<Book> booksInCart;
        booksInCart = new ArrayList<Book>(cart.getBooks());
        System.out.println(booksInCart);
        model.addAttribute("booksInCart", booksInCart);
        return "booksInCart";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String handleAddBookToCart(String isbn) {

        Book book = bookSearcher.findByISBN(isbn);
        cartHandler.addBook(book);
        return "redirect:/cart.html";
    }

    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public String handleRemoveBookFromCart(String isbn) {
        Book book = bookSearcher.findByISBN(isbn);
        cartHandler.removeBook(book);
        return "redirect:/cart.html";
    }
}
