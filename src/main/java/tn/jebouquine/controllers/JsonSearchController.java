package tn.jebouquine.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tn.jebouquine.pojo.Book;
import tn.jebouquine.pojo.forms.BookMultiCriteriaForm;
import tn.jebouquine.services.BookSearcher;

import java.util.List;

@RestController
@Transactional
public class JsonSearchController {
    @Autowired
    BookSearcher bookSearcher;

    @RequestMapping("/searchResultjson")
    List<Book> searchResults(@ModelAttribute("multiCriteriaForm") BookMultiCriteriaForm bookMultiCriteriaForm, Model model) {
//        System.out.println("search result:" + bookMultiCriteriaForm.getBookName());
        List<Book> books = bookSearcher.multiCriteriaSearch(bookMultiCriteriaForm);
        return books;
    }
}
