package tn.jebouquine.controllers.adddata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tn.jebouquine.pojo.Category;
import tn.jebouquine.services.CategoryHandler;

import javax.validation.Valid;

@Controller
@RequestMapping("/addCategory")
public class AddCategoryController {
    @Autowired
    private CategoryHandler categoryHandler;

    private static final String FORM_VIEW_NAME = "addCategory";
    private static final String REDIRECT_URL = "redirect:/index.html";

    @RequestMapping(method = RequestMethod.GET)
    public String showAddCategoryForm() {
        return FORM_VIEW_NAME;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String handleAddAuthorForm(@Valid Category category, Errors errors) {

        if (errors.hasErrors()) {
            return FORM_VIEW_NAME;
        }

        categoryHandler.save(category);
//        System.out.println(category);
        return REDIRECT_URL;
    }

}
