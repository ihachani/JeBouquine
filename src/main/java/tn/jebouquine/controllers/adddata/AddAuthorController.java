package tn.jebouquine.controllers.adddata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tn.jebouquine.pojo.Author;
import tn.jebouquine.services.AuthorHandler;

import javax.validation.Valid;

@Controller
@RequestMapping("/addAuthor")
public class AddAuthorController {
    @Autowired
    private AuthorHandler authorHandler;

    private static final String FORM_VIEW_NAME = "addAuthor";
    private static final String REDIRECT_URL = "redirect:/index.html";

    @RequestMapping(method = RequestMethod.GET)
    public String showAddAuthorForm() {

        return FORM_VIEW_NAME;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String handleAddAuthorForm(@Valid Author author, Errors errors) {

        if (errors.hasErrors()) {
            return FORM_VIEW_NAME;
        }

        authorHandler.save(author);
//        System.out.println(author);
        return REDIRECT_URL;
    }
}
