package tn.jebouquine.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by lonsomehell on 11/17/15.
 */
@Controller
public class HomeController {

    @RequestMapping(value = "/index")
    public String index(Model model){

        return "index";
    }
}
