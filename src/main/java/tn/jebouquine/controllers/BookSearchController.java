package tn.jebouquine.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import tn.jebouquine.pojo.Book;
import tn.jebouquine.pojo.forms.BookMultiCriteriaForm;
import tn.jebouquine.services.BookSearcher;

import java.util.ArrayList;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Created by lonsomehell on 11/20/15.
 */
@Controller
public class BookSearchController {

    @Autowired
    BookSearcher bookSearcher;

//    @Autowired
//    public BookSearchController(BookSearcher bookSearcher) {
//        this.bookSearcher = bookSearcher;
//    }

    @RequestMapping(value = "/searchTest", method = GET)
    public String showSearchPage(@ModelAttribute("multiCriteriaForm") BookMultiCriteriaForm bookMultiCriteriaForm) {
        return "searchTest";
    }

    @RequestMapping(value = "/searchResult", method = GET)
    public String searchResults(@ModelAttribute("multiCriteriaForm") BookMultiCriteriaForm bookMultiCriteriaForm,
                                Model model, @ModelAttribute("books") ArrayList<Book> books) {
        books = new ArrayList<Book>(bookSearcher.multiCriteriaSearch(bookMultiCriteriaForm));
        model.addAttribute("books", books);
        return "viewBooks";
    }

    @RequestMapping(value = "/allBooks", method = GET)
    public String searchResults(Model model, @ModelAttribute("books") ArrayList<Book> books) {
        books = new ArrayList<Book>(bookSearcher.getAll());
        model.addAttribute("books", books);
        return "viewBooks";
    }
//    @RequestMapping(value = "/users", method = RequestMethod.GET)
//    public String showAllUsers(Model model) {
//
//        logger.debug("showAllUsers()");
//        model.addAttribute("users", userService.findAll());
//        return "users/list";
//
//    }

}
