package tn.jebouquine.dao.jpa;

import org.springframework.stereotype.Repository;
import tn.jebouquine.dao.CommandDAO;
import tn.jebouquine.pojo.Account;
import tn.jebouquine.pojo.Book;
import tn.jebouquine.pojo.Command;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository("JPACommandDAO")
public class JPACommandDAO extends AbstractJPADAO<Command, Long> implements CommandDAO {


    @Override
    public List<Command> getUserCommands(Account user) {

        CriteriaQuery<Command> commandCriteriaQuery = createFindAllUserCommandsQuery(user);

        return runCriteriaQuery(commandCriteriaQuery);
    }

    private List<Command> runCriteriaQuery(CriteriaQuery<Command> commandCriteriaQuery) {

        TypedQuery<Command> query = em.createQuery(commandCriteriaQuery);

        List<Command> commands = query.getResultList();

//        System.out.println(query.unwrap(org.hibernate.Query.class).getQueryString());

        return commands;
    }

    private CriteriaQuery<Command> createFindAllUserCommandsQuery(Account account) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Command> commandCriteriaQuery = criteriaBuilder.createQuery(Command.class);
        Root<Command> commandRoot = commandCriteriaQuery.from(Command.class);

        Join<Book, Account> bookAccountJoin = commandRoot.join("account");
        commandCriteriaQuery.where(criteriaBuilder.equal(bookAccountJoin, account));
        return commandCriteriaQuery;
    }
}
