/**
 * Package contains jpa implementation of the DAO interfaces.
 */
package tn.jebouquine.dao.jpa;

import tn.jebouquine.dao.GenericDAO;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.lang.reflect.ParameterizedType;

public abstract class AbstractJPADAO<E, K> implements GenericDAO<E, K> {

    @PersistenceContext
    protected EntityManager em;

    protected Class<E> entityClass;

    public AbstractJPADAO() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass()
                .getGenericSuperclass();
        this.entityClass = (Class<E>)
                genericSuperclass.getActualTypeArguments()[0];
    }

    @Override
    public E create(E entity) {
        em.persist(entity);
        return entity;
    }

    @Override
    public E retrieve(K key) {
        return em.find(entityClass, key);
    }

    @Override
    public void update(E entity) {
        em.merge(entity);
    }

    @Override
    public void delete(E entity) {
        em.remove(entity);
    }
}
