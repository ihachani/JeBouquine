package tn.jebouquine.dao.jpa;

import org.springframework.stereotype.Repository;
import tn.jebouquine.dao.BookDAO;
import tn.jebouquine.pojo.Author;
import tn.jebouquine.pojo.Book;
import tn.jebouquine.pojo.Category;
import tn.jebouquine.pojo.forms.BookMultiCriteriaForm;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lonsomehell on 11/17/15.
 */
//TODO WHAT A MESS
@Repository("JPABookDAO")
public class JPABookDAO extends AbstractJPADAO<Book, String> implements BookDAO {
    @Override
    public List<Book> findAll() {
        CriteriaQuery<Book> bookCriteriaQuery = createFindAllBooksCriteriaQuery();

        return runCriteriaQuery(bookCriteriaQuery);
    }


    private CriteriaQuery<Book> createFindAllBooksCriteriaQuery() {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Book> bookCriteriaQuery = criteriaBuilder.createQuery(Book.class);
        Root<Book> bookRoot = bookCriteriaQuery.from(Book.class);
        bookCriteriaQuery.select(bookRoot);
        return bookCriteriaQuery;
    }

    private List<Book> runCriteriaQuery(CriteriaQuery<Book> bookCriteriaQuery) {

        TypedQuery<Book> query = em.createQuery(bookCriteriaQuery);

        List<Book> books = query.getResultList();

//        System.out.println(query.unwrap(org.hibernate.Query.class).getQueryString());

        return books;
    }

    @Override
    public List<Book> findByCategory(Category category) {

        CriteriaQuery<Book> bookCriteriaQuery = createFindByCategoryBooksCriteriaQuery(category);

        return runCriteriaQuery(bookCriteriaQuery);
    }

    private CriteriaQuery<Book> createFindByCategoryBooksCriteriaQuery(Category category) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Book> bookCriteriaQuery = criteriaBuilder.createQuery(Book.class);
        Root<Book> bookRoot = bookCriteriaQuery.from(Book.class);

        Join<Book, Category> joinCategory = bookRoot.join("categories");
        bookCriteriaQuery.where(criteriaBuilder.equal(joinCategory, category));
        return bookCriteriaQuery;
    }

    public List<Book> fastSearch(String searchTerm) {
        CriteriaQuery<Book> bookCriteriaQuery = createFastSearchBooksCriteriaQuery(searchTerm);

        return runCriteriaQuery(bookCriteriaQuery);
    }

    private CriteriaQuery<Book> createFastSearchBooksCriteriaQuery(String searchTerm) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Book> bookCriteriaQuery = criteriaBuilder.createQuery(Book.class);
        Root<Book> bookRoot = bookCriteriaQuery.from(Book.class);

        Expression<String> literal = criteriaBuilder.lower(criteriaBuilder.literal((String) "%" + searchTerm + "%"));

        bookCriteriaQuery.where(
                criteriaBuilder.or
                        (criteriaBuilder.or(
                                criteriaBuilder.like(criteriaBuilder.lower(bookRoot.<String>get("ISBN")), literal),
                                criteriaBuilder.like(criteriaBuilder.lower(bookRoot.<String>get("name")), literal)
                                ),
                                criteriaBuilder.like(criteriaBuilder.lower(bookRoot.<String>get("description")), literal)
                        )
        );
        return bookCriteriaQuery;
    }

    public List<Book> multiCriteriaSearch(BookMultiCriteriaForm searchCriterias) {
        CriteriaQuery<Book> bookCriteriaQuery = createMultiCriteriaSearchBooksCriteriaQuery(searchCriterias);

        return runCriteriaQuery(bookCriteriaQuery);
    }

    private CriteriaQuery<Book> createMultiCriteriaSearchBooksCriteriaQuery(BookMultiCriteriaForm searchCriterias) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Book> bookCriteriaQuery = criteriaBuilder.createQuery(Book.class);

        Root<Book> bookRoot = bookCriteriaQuery.from(Book.class);

        Join<Book, Category> joinCategory = bookRoot.join("categories");
        Join<Book, Author> joinAuthor = bookRoot.join("authors");

        List<Predicate> predicates = new ArrayList<Predicate>();

        if (searchCriterias.getISBN() != null && !searchCriterias.getISBN().isEmpty()) {
            predicates.add(
                    criteriaBuilder.like(bookRoot.<String>get("ISBN"), searchCriterias.getISBN()));
        }
        if (searchCriterias.getBookName() != null && !searchCriterias.getBookName().isEmpty()) {
            predicates.add(
                    criteriaBuilder.like(bookRoot.<String>get("name"), searchCriterias.getBookName()));
        }
        if (searchCriterias.getAuthorName() != null && !searchCriterias.getAuthorName().isEmpty()) {
            predicates.add(
                    criteriaBuilder.equal(joinAuthor.get("name"), searchCriterias.getAuthorName()));
        }
        if (searchCriterias.getCategoryName() != null && !searchCriterias.getCategoryName().isEmpty()) {
            predicates.add(
                    criteriaBuilder.equal(joinCategory.get("name"), searchCriterias.getCategoryName()));
        }

        bookCriteriaQuery.select(bookRoot)
                .where(predicates.toArray(new Predicate[]{}))
                .distinct(true);
        return bookCriteriaQuery;
    }

    @Override
    public void deleteAll() {
        List<Book> books = findAll();
        for (Book book : books) {
            em.remove(book);
        }
    }

}
