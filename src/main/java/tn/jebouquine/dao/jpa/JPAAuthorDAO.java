package tn.jebouquine.dao.jpa;

import org.springframework.stereotype.Repository;
import tn.jebouquine.dao.AuthorDAO;
import tn.jebouquine.pojo.Author;

@Repository("JPAAuthorDAO")
public class JPAAuthorDAO extends AbstractJPADAO<Author, Long> implements AuthorDAO {
}
