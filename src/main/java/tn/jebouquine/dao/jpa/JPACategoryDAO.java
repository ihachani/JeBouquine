package tn.jebouquine.dao.jpa;

import org.springframework.stereotype.Repository;
import tn.jebouquine.dao.CategoryDAO;
import tn.jebouquine.pojo.Category;

@Repository("JPACategoryDAO")
public class JPACategoryDAO extends AbstractJPADAO<Category, Long>
        implements CategoryDAO {
}
