package tn.jebouquine.dao.jpa;

import org.springframework.stereotype.Repository;
import tn.jebouquine.dao.CartDAO;
import tn.jebouquine.pojo.Cart;

/**
 * Created by lonsomehell on 11/19/15.
 */
@Repository("JPACartDAO")
public class JPACartDAO extends AbstractJPADAO<Cart,Long> implements CartDAO{
    @Override
    public void delete(Cart entity) {
        entity.getBooks().removeAll(entity.getBooks());
        super.delete(entity);
    }
}
