package tn.jebouquine.dao.jpa;

import org.springframework.stereotype.Repository;
import tn.jebouquine.dao.AccountDAO;
import tn.jebouquine.pojo.Account;

@Repository("JPAAccountDAO")
public class JPAAccountDAO extends AbstractJPADAO<Account, String> implements AccountDAO {
}
