package tn.jebouquine.dao;

import tn.jebouquine.pojo.Delivery;

/**
 * Created by lonsomehell on 10/23/15.
 */
public interface DeliveryDAO extends GenericDAO<Delivery,Long> {

}
