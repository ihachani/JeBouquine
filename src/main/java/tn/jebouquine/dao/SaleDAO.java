package tn.jebouquine.dao;

import tn.jebouquine.pojo.Sale;

/**
 * Created by lonsomehell on 10/23/15.
 */
public interface SaleDAO extends GenericDAO<Sale,Long> {
}
