package tn.jebouquine.dao;

import tn.jebouquine.pojo.Category;

/**
 * Created by lonsomehell on 10/23/15.
 */
public interface CategoryDAO extends GenericDAO<Category,Long>{

}
