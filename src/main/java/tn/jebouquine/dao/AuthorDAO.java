package tn.jebouquine.dao;

import tn.jebouquine.pojo.Author;

/**
 * Created by lonsomehell on 10/23/15.
 */
public interface AuthorDAO extends GenericDAO<Author,Long> {
}
