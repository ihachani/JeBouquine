package tn.jebouquine.dao;

import tn.jebouquine.pojo.Invoice;

/**
 * Created by lonsomehell on 10/23/15.
 */
public interface InvoiceDAO extends GenericDAO<Invoice,Long>{
}
