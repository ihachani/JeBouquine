package tn.jebouquine.dao;

import tn.jebouquine.pojo.Account;

/**
 * Created by lonsomehell on 10/23/15.
 */
public interface AccountDAO extends GenericDAO<Account,String>{
}
