package tn.jebouquine.dao;

import tn.jebouquine.pojo.Cart;

/**
 * Created by lonsomehell on 10/23/15.
 */
public interface CartDAO extends GenericDAO<Cart,Long>{
}
