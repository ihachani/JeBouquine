package tn.jebouquine.dao;

import tn.jebouquine.pojo.Book;
import tn.jebouquine.pojo.Category;
import tn.jebouquine.pojo.forms.BookMultiCriteriaForm;

import java.util.List;
import java.util.Map;

/**
 * Created by lonsomehell on 10/23/15.
 */
public interface BookDAO extends GenericDAO<Book,String> {
    List<Book> findAll();
    List<Book> findByCategory(Category category);
    List<Book> fastSearch(String searchTerm);
    List<Book> multiCriteriaSearch(BookMultiCriteriaForm searchCriterias);
    void deleteAll();
}
