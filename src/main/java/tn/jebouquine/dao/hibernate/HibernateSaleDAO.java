package tn.jebouquine.dao.hibernate;

import org.hibernate.Session;
import tn.jebouquine.dao.SaleDAO;
import tn.jebouquine.hiberanteutilites.SessionUtilities;
import tn.jebouquine.pojo.Sale;

/**
 * Created by lonsomehell on 10/29/15.
 */
public class HibernateSaleDAO implements SaleDAO {

    @Override
    public Sale create(Sale sale) {
        Session session = SessionUtilities.openSessionAndBeginTransaction();

        session.save(sale);

        SessionUtilities.commitAndCloseSession(session);
        return sale;
    }

    @Override
    public Sale retrieve(Long id) {
        Session session = SessionUtilities.openSessionAndBeginTransaction();

        Sale sale = session.get(Sale.class , id);

        SessionUtilities.commitAndCloseSession(session);
        return sale;
    }

    @Override
    public void update(Sale sale) {
        Session session = SessionUtilities.openSessionAndBeginTransaction();

        session.update(sale);

        SessionUtilities.commitAndCloseSession(session);
    }

    @Override
    public void delete(Sale sale) {
        Session session = SessionUtilities.openSessionAndBeginTransaction();

        session.delete(sale);

        SessionUtilities.commitAndCloseSession(session);
    }
}
