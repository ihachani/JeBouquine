package tn.jebouquine.dao.hibernate;

import org.hibernate.Session;
import tn.jebouquine.dao.DeliveryDAO;
import tn.jebouquine.hiberanteutilites.SessionUtilities;
import tn.jebouquine.pojo.Delivery;

/**
 * Created by lonsomehell on 10/29/15.
 */
public class HibernateDeliveryDAO implements DeliveryDAO {
    @Override
    public Delivery create(Delivery delivery) {
        Session session = SessionUtilities.openSessionAndBeginTransaction();

        session.save(delivery);

        SessionUtilities.commitAndCloseSession(session);
        return delivery;
    }

    @Override
    public Delivery retrieve(Long id) {
        Session session = SessionUtilities.openSessionAndBeginTransaction();

        Delivery delivery = session.get(Delivery.class , id);

        SessionUtilities.commitAndCloseSession(session);
        return delivery;
    }

    @Override
    public void update(Delivery delivery) {
        Session session = SessionUtilities.openSessionAndBeginTransaction();

        session.update(delivery);

        SessionUtilities.commitAndCloseSession(session);
    }

    @Override
    public void delete(Delivery delivery) {
        Session session = SessionUtilities.openSessionAndBeginTransaction();

        session.delete(delivery);

        SessionUtilities.commitAndCloseSession(session);
    }
}
