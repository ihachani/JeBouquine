package tn.jebouquine.dao.hibernate;

import org.hibernate.Session;
import tn.jebouquine.dao.AccountDAO;
import tn.jebouquine.hiberanteutilites.SessionUtilities;
import tn.jebouquine.pojo.Account;

/**
 * Created by lonsomehell on 10/23/15.
 */
public class HibernateAccountDAO implements AccountDAO{

    @Override
    public Account create(Account account) {
        Session session = SessionUtilities.openSessionAndBeginTransaction();

        session.save(account);

        SessionUtilities.commitAndCloseSession(session);
        return account;
    }

    @Override
    public Account retrieve(String login) {
        Session session = SessionUtilities.openSessionAndBeginTransaction();

        Account account = session.get(Account.class , login);

        SessionUtilities.commitAndCloseSession(session);
        return account;
    }

    @Override
    public void update(Account account) {
        Session session = SessionUtilities.openSessionAndBeginTransaction();

        session.update(account);

        SessionUtilities.commitAndCloseSession(session);
    }


    @Override
    public void delete(Account account) {
        Session session = SessionUtilities.openSessionAndBeginTransaction();

        session.delete(account);

        SessionUtilities.commitAndCloseSession(session);
    }
}
