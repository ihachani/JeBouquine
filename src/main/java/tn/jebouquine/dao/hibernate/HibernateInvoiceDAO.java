package tn.jebouquine.dao.hibernate;

import org.hibernate.Session;
import tn.jebouquine.dao.InvoiceDAO;
import tn.jebouquine.hiberanteutilites.SessionUtilities;
import tn.jebouquine.pojo.Invoice;

/**
 * Created by lonsomehell on 10/29/15.
 */
public class HibernateInvoiceDAO implements InvoiceDAO {
    @Override
    public Invoice create(Invoice invoice) {
        Session session = SessionUtilities.openSessionAndBeginTransaction();

        session.save(invoice);

        SessionUtilities.commitAndCloseSession(session);
        return invoice;
    }

    @Override
    public Invoice retrieve(Long id) {
        Session session = SessionUtilities.openSessionAndBeginTransaction();

        Invoice invoice = session.get(Invoice.class , id);

        SessionUtilities.commitAndCloseSession(session);
        return invoice;
    }

    @Override
    public void update(Invoice invoice) {
        Session session = SessionUtilities.openSessionAndBeginTransaction();

        session.update(invoice);

        SessionUtilities.commitAndCloseSession(session);
    }

    @Override
    public void delete(Invoice invoice) {
        Session session = SessionUtilities.openSessionAndBeginTransaction();

        session.delete(invoice);

        SessionUtilities.commitAndCloseSession(session);
    }
}
