package tn.jebouquine.dao.hibernate;

import org.hibernate.Session;
import tn.jebouquine.dao.AuthorDAO;
import tn.jebouquine.hiberanteutilites.SessionUtilities;
import tn.jebouquine.pojo.Author;

/**
 * Created by lonsomehell on 10/23/15.
 */
@Deprecated
public class HibernateAuthorDAO implements AuthorDAO{
    @Override
    public Author create(Author author) {
        Session session = SessionUtilities.openSessionAndBeginTransaction();

        session.save(author);

        SessionUtilities.commitAndCloseSession(session);
        return author;
    }

    @Override
    public Author retrieve(Long id) {
        Session session = SessionUtilities.openSessionAndBeginTransaction();

        Author author = session.get(Author.class , id);

        SessionUtilities.commitAndCloseSession(session);
        return author;
    }

    @Override
    public void update(Author author) {
        Session session = SessionUtilities.openSessionAndBeginTransaction();

        session.update(author);

        SessionUtilities.commitAndCloseSession(session);
    }

    @Override
    public void delete(Author author) {
        Session session = SessionUtilities.openSessionAndBeginTransaction();

        session.delete(author);

        SessionUtilities.commitAndCloseSession(session);
    }
}
