package tn.jebouquine.dao.hibernate;

import org.hibernate.Session;
import tn.jebouquine.dao.CategoryDAO;
import tn.jebouquine.hiberanteutilites.SessionUtilities;
import tn.jebouquine.pojo.Category;

/**
 * Created by lonsomehell on 10/29/15.
 */
@Deprecated
public class HibernateCategoryDAO implements CategoryDAO {
    @Override
    public Category create(Category category) {
        Session session = SessionUtilities.openSessionAndBeginTransaction();

        session.save(category);

        SessionUtilities.commitAndCloseSession(session);
        return category;
    }

    @Override
    public Category retrieve(Long id) {
        Session session = SessionUtilities.openSessionAndBeginTransaction();

        Category category = session.get(Category.class, id);

        SessionUtilities.commitAndCloseSession(session);
        return category;
    }

    @Override
    public void update(Category category) {
        Session session = SessionUtilities.openSessionAndBeginTransaction();

        session.update(category);

        SessionUtilities.commitAndCloseSession(session);
    }

    @Override
    public void delete(Category category) {
        Session session = SessionUtilities.openSessionAndBeginTransaction();

        session.delete(category);

        SessionUtilities.commitAndCloseSession(session);
    }
}
