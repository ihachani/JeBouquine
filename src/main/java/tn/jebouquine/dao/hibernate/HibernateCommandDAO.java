package tn.jebouquine.dao.hibernate;

import org.hibernate.Session;
import tn.jebouquine.dao.CommandDAO;
import tn.jebouquine.hiberanteutilites.SessionUtilities;
import tn.jebouquine.pojo.Account;
import tn.jebouquine.pojo.Command;

import java.util.List;

/**
 * Created by lonsomehell on 10/29/15.
 */
@Deprecated
public class HibernateCommandDAO implements CommandDAO {
    @Override
    public Command create(Command command) {
        Session session = SessionUtilities.openSessionAndBeginTransaction();

        session.save(command);

        SessionUtilities.commitAndCloseSession(session);
        return command;
    }

    @Override
    public Command retrieve(Long id) {
        Session session = SessionUtilities.openSessionAndBeginTransaction();

        Command command = session.get(Command.class, id);

        SessionUtilities.commitAndCloseSession(session);
        return command;
    }

    @Override
    public void update(Command command) {
        Session session = SessionUtilities.openSessionAndBeginTransaction();

        session.update(command);

        SessionUtilities.commitAndCloseSession(session);
    }

    @Override
    public void delete(Command command) {
        Session session = SessionUtilities.openSessionAndBeginTransaction();

        session.delete(command);

        SessionUtilities.commitAndCloseSession(session);
    }

    @Override
    public List<Command> getUserCommands(Account user) {
        return null;
    }
}
