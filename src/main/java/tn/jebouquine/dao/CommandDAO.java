package tn.jebouquine.dao;

import tn.jebouquine.pojo.Account;
import tn.jebouquine.pojo.Command;

import java.util.List;

/**
 * Created by lonsomehell on 10/23/15.
 */
public interface CommandDAO extends GenericDAO<Command,Long>{
    List<Command> getUserCommands(Account user);
}
