package tn.jebouquine.dao;

/**
 * Created by lonsomehell on 11/18/15.
 */
public interface GenericDAO<E,K> {
    E create(E entity);
    E retrieve(K key);
    void update(E entity);
    void delete(E  entity);
}
