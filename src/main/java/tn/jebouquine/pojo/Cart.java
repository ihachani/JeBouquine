package tn.jebouquine.pojo;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lonsomehell on 10/22/15.
 */
@Entity
public class Cart {
    public Cart() {
        total = new BigDecimal("0");
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @NotNull
    private BigDecimal total;
    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE})
    @OrderColumn(name = "INDEX")
    @NotEmpty
    private List<Book> books = new ArrayList<Book>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public void addBook(Book book) {
        books.add(book);
        getTotalPrice();
    }

    public void removeBook(Book book) {
        books.remove(book);
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getTotalPrice() {
        BigDecimal totalPrice = new BigDecimal("0");
        for (Book book : this.getBooks()
                ) {
            totalPrice = totalPrice.add(book.getPrice());
//            System.out.println("Cart.getTotalPrice");
//            System.out.println(totalPrice);
//            System.out.println(this);
        }
        total = totalPrice;
        return totalPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cart)) return false;

        Cart cart = (Cart) o;

        if (getId() != cart.getId()) return false;
        if (getTotal() != null ? !getTotal().equals(cart.getTotal()) : cart.getTotal() != null) return false;
        return getBooks() != null ? getBooks().equals(cart.getBooks()) : cart.getBooks() == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (getId() ^ (getId() >>> 32));
        result = 31 * result + (getTotal() != null ? getTotal().hashCode() : 0);
        result = 31 * result + (getBooks() != null ? getBooks().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Cart{" +
                "id=" + id +
                ", total=" + total +
                ", books=" + books +
                '}';
    }
}
