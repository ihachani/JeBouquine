package tn.jebouquine.pojo;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by lonsomehell on 10/22/15.
 */
@Entity
public class Command {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @NotNull
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime date;
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @NotNull
    private Cart cart;
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @NotNull
    private Account account;
//    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    @NotNull
//    private Sale sale;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
    //    public Sale getSale() {
//        return sale;
//    }
//
//    public void setSale(Sale sale) {
//        this.sale = sale;
//    }

    @Override
    public String toString() {
        return "Command{" +
                "id=" + id +
                ", date=" + date +
                ", cart=" + cart +
                ", account=" + account +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Command)) return false;

        Command command = (Command) o;

        if (getId() != command.getId()) return false;
        if (getDate() != null ? !getDate().equals(command.getDate()) : command.getDate() != null) return false;
        if (getCart() != null ? !getCart().equals(command.getCart()) : command.getCart() != null) return false;
        return getAccount() != null ? getAccount().equals(command.getAccount()) : command.getAccount() == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (getId() ^ (getId() >>> 32));
        result = 31 * result + (getDate() != null ? getDate().hashCode() : 0);
        result = 31 * result + (getCart() != null ? getCart().hashCode() : 0);
        result = 31 * result + (getAccount() != null ? getAccount().hashCode() : 0);
        return result;
    }
}
