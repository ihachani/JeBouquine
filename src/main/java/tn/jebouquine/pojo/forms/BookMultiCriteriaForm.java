package tn.jebouquine.pojo.forms;

/**
 * Created by lonsomehell on 11/20/15.
 */
public class BookMultiCriteriaForm {
    private String ISBN;
    private String bookName;
    private String authorName;
    private String categoryName;

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BookMultiCriteriaForm)) return false;

        BookMultiCriteriaForm that = (BookMultiCriteriaForm) o;

        if (getISBN() != null ? !getISBN().equals(that.getISBN()) : that.getISBN() != null) return false;
        if (getBookName() != null ? !getBookName().equals(that.getBookName()) : that.getBookName() != null)
            return false;
        if (getAuthorName() != null ? !getAuthorName().equals(that.getAuthorName()) : that.getAuthorName() != null)
            return false;
        return !(getCategoryName() != null ? !getCategoryName().equals(that.getCategoryName()) : that.getCategoryName() != null);

    }

    @Override
    public int hashCode() {
        int result = getISBN() != null ? getISBN().hashCode() : 0;
        result = 31 * result + (getBookName() != null ? getBookName().hashCode() : 0);
        result = 31 * result + (getAuthorName() != null ? getAuthorName().hashCode() : 0);
        result = 31 * result + (getCategoryName() != null ? getCategoryName().hashCode() : 0);
        return result;
    }
}
