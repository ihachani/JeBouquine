package tn.jebouquine.pojo;

import javax.persistence.Embeddable;

/**
 * Created by lonsomehell on 10/22/15.
 */
@Embeddable
public class BookComment {
    private String comment;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BookComment)) return false;

        BookComment that = (BookComment) o;

        if (!comment.equals(that.comment)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return comment.hashCode();
    }
}
