package tn.jebouquine.pojo;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@NamedQueries(
        @NamedQuery(name = "Book.DeleteAll", query = "DELETE FROM Book")
)
@Entity
public class Book {
    @Id
    private String ISBN;
    @NotEmpty
    private String name;
    @NotNull
    @DecimalMin(value = "0.00")
    private BigDecimal price;
    private String description;
    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @OrderColumn(name = "INDEX")
    @NotEmpty
    private List<Author> authors = new ArrayList<Author>();
    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @OrderColumn(name = "INDEX")
    private List<Category> categories = new ArrayList<Category>();
    @ElementCollection(fetch = FetchType.EAGER)
    private Set<BookComment> bookComments = new HashSet<BookComment>();

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public Set<BookComment> getBookComments() {
        return bookComments;
    }

    public void setBookComments(Set<BookComment> bookComments) {
        this.bookComments = bookComments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;

        Book book = (Book) o;

        if (!ISBN.equals(book.ISBN)) return false;
        if (!this.getAuthors().equals(book.getAuthors())) return false;
        if (this.getBookComments() != null ? !this.getBookComments().equals(book.getBookComments()) : book.getBookComments() != null)
            return false;
        if (this.getCategories() != null ? !this.getCategories().equals(book.getCategories()) : book.getCategories() != null)
            return false;
        if (description != null ? !description.equals(book.description) : book.description != null) return false;
        if (!name.equals(book.name)) return false;
        if (!price.equals(book.price)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ISBN.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + price.hashCode();
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (authors != null ? authors.hashCode() : 0);
        result = 31 * result + (categories != null ? categories.hashCode() : 0);
        result = 31 * result + (bookComments != null ? bookComments.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        String bookString = "Book \n" +
                "ISBN:" + ISBN + "\n" +
                "Name:" + name + "\n" +
                "Price:" + price + "\n" +
                "Description:" + description + "\n" +
                "Authors:" + authors + "\n" +
                "Categories:" + categories + "\n" +
                "bookComments:" + bookComments + "\n";
        return bookString;
    }
}
