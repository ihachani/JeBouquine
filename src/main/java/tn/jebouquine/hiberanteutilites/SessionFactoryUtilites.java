package tn.jebouquine.hiberanteutilites;

import org.hibernate.HibernateError;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by lonsomehell on 10/22/15.
 */
public class SessionFactoryUtilites {

    private static SessionFactory sessionFactory;

    //Static Initialization Block.
    static {
        try {
            Logger logger = LoggerFactory.getLogger(SessionFactoryUtilites.class);

            logger.info("Creating StandardServiceRegistry");
            StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder()
                    .configure("hibernate.cfg.xml")
                    .build();

            logger.info("Creating Metadata");
            Metadata metadata = new MetadataSources(standardRegistry)
                    .getMetadataBuilder()
                    .build();

            // builds a session factory from the MetaData
            logger.info("Building sessionFactory from Metadata");
            sessionFactory = metadata.getSessionFactoryBuilder().build();
        } catch (HibernateError error) {
            Logger logger = LoggerFactory.getLogger(SessionFactoryUtilites.class);
            logger.error("Session was not created", error);
        } catch (Exception error) {
            Logger logger = LoggerFactory.getLogger(SessionFactoryUtilites.class);
            logger.error("Session was not created", error);
        }


    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static void setSessionFactory(SessionFactory sessionFactory) {
        SessionFactoryUtilites.sessionFactory = sessionFactory;
    }

    public static void shutdown() {
        getSessionFactory().close();
    }
}
