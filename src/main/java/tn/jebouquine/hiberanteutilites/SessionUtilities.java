package tn.jebouquine.hiberanteutilites;

import org.hibernate.Session;

/**
 * Created by lonsomehell on 10/23/15.
 */
public class SessionUtilities {
    public static Session openSession() {
        return SessionFactoryUtilites.getSessionFactory().openSession();
    }

    public static Session openSessionAndBeginTransaction() {
        Session session = openSession();
        session.beginTransaction();
        return session;
    }

    public static void commitAndCloseSession(Session session) {
        session.getTransaction().commit();
        closeSession(session);
    }

    public static void closeSession(Session session) {
        session.close();
    }
}
