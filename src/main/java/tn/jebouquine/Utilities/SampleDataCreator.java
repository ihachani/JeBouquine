package tn.jebouquine.Utilities;

import org.joda.time.DateTime;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import tn.jebouquine.pojo.*;

import java.math.BigDecimal;
import java.util.ArrayList;

@Component
@Scope(value = "prototype")
public class SampleDataCreator {
    private Book book;
    private Book book2;

    private Author author;
    private Author author1;

    private Category category;
    private Category category1;

    private BookComment bookComment;
    private BookComment bookComment1;

    private Account adminAccount;
    private Account userAccount;

    private Cart cart;
    private Command command;
    private Command command1;
    private Command command2;

    private final String authorName = "The lonely developer";
    private final String author1Name = "R.C.Martin";

    public SampleDataCreator() {

        author = new Author();
        author.setName(authorName);
        author1 = new Author();
        author1.setName(author1Name);

        category = new Category();
        category.setName("IT");
        category1 = new Category();
        category1.setName("Software Engineering");


        bookComment = new BookComment();
        bookComment.setComment("Yahoo!!");
        bookComment1 = new BookComment();
        bookComment1.setComment("A great book");

        book = new Book();
        book.setISBN("isbn:9780137081076");
        book.setName("Book1");
        book.setPrice(new BigDecimal("10.00"));
        book.getAuthors().add(author);
        book.getAuthors().add(author1);
        book.getCategories().add(category);
        book.getCategories().add(category1);
        book.getBookComments().add(bookComment);
        book.getBookComments().add(bookComment1);

        book2 = new Book();
        book2.setISBN("isbn:9780137081073");
        book2.setName("Book2");
        book2.setPrice(new BigDecimal("10.00"));
        book2.getAuthors().add(author);
        book2.getAuthors().add(author1);
        book2.getCategories().add(category);
        book2.getBookComments().add(bookComment);
        book2.getBookComments().add(bookComment1);

        adminAccount = new Account();
        adminAccount.setName("administrator");
        adminAccount.setLogin("admin");
        adminAccount.setPassword("password");
        adminAccount.setEmail("admin@jebouquine.tn");
        ArrayList<String> adminRoles = new ArrayList<String>();
        adminRoles.add("ROLE_ADMIN");
        adminRoles.add("ROLE_USER");
        adminAccount.setRoles(adminRoles);


        userAccount = new Account();
        userAccount.setName("user");
        userAccount.setLogin("user");
        userAccount.setPassword("password");
        userAccount.setEmail("user@jebouquine.tn");
        ArrayList<String> userRoles = new ArrayList<String>();
        userRoles.add("ROLE_USER");
        userAccount.setRoles(userRoles);

        cart = new Cart();
        cart.addBook(book);
        cart.addBook(book2);
        cart.addBook(book);

        command = new Command();
        command.setAccount(userAccount);
        command.setCart(cart);
        command.setDate(new DateTime());

        command1 = new Command();
        command1.setAccount(userAccount);
        command1.setCart(cart);
        command1.setDate(new DateTime());

        command2 = new Command();
        command2.setAccount(adminAccount);
        command2.setCart(cart);
        command2.setDate(new DateTime());
    }

    public Book getBook() {
        return book;
    }

    public Book getBook2() {
        return book2;
    }

    public Author getAuthor() {
        return author;
    }

    public Author getAuthor1() {
        return author1;
    }

    public Category getCategory() {
        return category;
    }

    public Category getCategory1() {
        return category1;
    }

    public BookComment getBookComment() {
        return bookComment;
    }

    public BookComment getBookComment1() {
        return bookComment1;
    }

    public Account getAdminAccount() {
        return adminAccount;
    }

    public Account getUserAccount() {
        return userAccount;
    }

    public Cart getCart() {
        return cart;
    }

    public Command getCommand() {
        return command;
    }

    public Command getCommand1() {
        return command1;
    }

    public Command getCommand2() {
        return command2;
    }
}
