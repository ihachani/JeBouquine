package tn.jebouquine.Utilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by lonsomehell on 10/23/15.
 * Class for Hashing strings.
 */
public class Hash {
    /**
     * Generates a sha512 out of the provided string.
     *
     * @param stringToHash
     * @return the sha512 hashed string
     */
    public static String sha512(String stringToHash) {
        try {
            byte[] digest = generateDigest(stringToHash);

            String hexStr = generatedHashedString(digest);
            return hexStr;

        } catch (NoSuchAlgorithmException exception) {
            Logger logger = LoggerFactory.getLogger(Hash.class);
            logger.error("Algorithm not found", exception);
            return null;
        } catch (UnsupportedEncodingException exception) {
            Logger logger = LoggerFactory.getLogger(Hash.class);
            logger.error("UnsupportedEncodingException while hashing", exception);
            return null;
        }
    }

    private static byte[] generateDigest(String stringToHash) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
        messageDigest.reset();
        byte[] buffer = stringToHash.getBytes();
        messageDigest.update(buffer);
        return messageDigest.digest();
    }

    private static String generatedHashedString(byte[] digest) {
        StringBuffer stringBuffer = new StringBuffer();
        String hexStr;
        for (int i = 0; i < digest.length; i++) {
//            hexStr += Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1);
            stringBuffer.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
        }
        hexStr = stringBuffer.toString();
        return hexStr;
    }


}
