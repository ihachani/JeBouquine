package tn.jebouquine.config;


import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import tn.jebouquine.repository.DBInitializer;

import javax.annotation.PostConstruct;

/**
 * Created by lonsomehell on 11/21/15.
 */
//@WebListener
@Component
public class StartupDBInitializer {
    @Autowired
    private DBInitializer dbInitializer;
    private Logger logger = org.slf4j.LoggerFactory.getLogger(StartupDBInitializer.class);

    @Transactional
    @PostConstruct
    public void init() {
        logger.trace("Started Initializing Database.");
        dbInitializer.setupDB();
        logger.trace("Database Initialized.");
    }
//    @Transactional
//    @Override
//    public void onApplicationEvent(ContextRefreshedEvent event) {
//
//    }
}
