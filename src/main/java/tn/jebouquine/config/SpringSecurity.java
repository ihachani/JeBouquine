package tn.jebouquine.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import tn.jebouquine.services.security.AccountService;

@Configuration
@EnableWebSecurity
@ComponentScan({"tn.jebouquine.dao"})
public class SpringSecurity extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .formLogin()
                .loginPage("/login.html")
                .and()
                .rememberMe()
                .tokenValiditySeconds(2419200)
                .key("jebouquineUserKey")
                .and()
                .authorizeRequests()
                .antMatchers("/login.html").anonymous()
                .antMatchers("/command/**").authenticated()
                .antMatchers("/cart/**").authenticated()
                .antMatchers("/cart*").authenticated()
//                .antMatchers(HttpMethod.POST, "/spittles").authenticated()
                .anyRequest().permitAll();
//        http
//                .authorizeRequests()
//                .anyRequest().authenticated()
//                .and()
//                .formLogin().and()
//                .httpBasic();
    }

    //    @Autowired
//    AccountDAO accountDAO;
    @Autowired
    AccountService accountService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth)
            throws Exception {
        auth.userDetailsService(accountService);
    }
}
