package tn.jebouquine.config;

import org.springframework.context.annotation.*;
import tn.jebouquine.pojo.Cart;

/**
 * Created by lonsomehell on 11/17/15.
 */

//TODO Seperate into smaller files.
@Configuration
@ComponentScan(basePackages = "tn.jebouquine.config ,tn.jebouquine.dao , tn.jebouquine.services ,tn.jebouquine.repository, tn.jebouquine.Utilities")
public class SpringConfig {

    @Bean
    @Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
    public Cart cart() {
        return new Cart();
    }
}
