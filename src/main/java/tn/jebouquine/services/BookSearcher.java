package tn.jebouquine.services;

import tn.jebouquine.pojo.Book;
import tn.jebouquine.pojo.Category;
import tn.jebouquine.pojo.forms.BookMultiCriteriaForm;

import java.util.List;

/**
 * Created by lonsomehell on 11/21/15.
 */
public interface BookSearcher {
    Book findByISBN(String isbn);

    List<Book> findByCategory(Category category);

    List<Book> fastSearch(String searchTerm);

    List<Book> multiCriteriaSearch(BookMultiCriteriaForm searchCriterias);

    List<Book> getAll();
}
