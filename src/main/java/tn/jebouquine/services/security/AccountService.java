package tn.jebouquine.services.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.jebouquine.dao.AccountDAO;
import tn.jebouquine.pojo.Account;

import java.util.ArrayList;
import java.util.List;

@Service("accountService")
public class AccountService implements UserDetailsService {

    private final AccountDAO accountDAO;

    @Autowired
    public AccountService(AccountDAO accountDAO) {
        this.accountDAO = accountDAO;
    }

//    @Transactional
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        System.out.println("AccountService.loadUserByUsername");
        System.out.println(accountDAO);
        Account account = accountDAO.retrieve(username);
        if (account != null) {
            List<GrantedAuthority> authorities = generateGrantedAuthorities(account);
            return createUser(account, authorities);

        }
        throw new UsernameNotFoundException(
                "User '" + username + "' not found.");

    }

    private List<GrantedAuthority> generateGrantedAuthorities(Account account) {
        List<GrantedAuthority> authorities =
                new ArrayList<GrantedAuthority>();
        for (String role : account.getRoles()
                ) {
            authorities.add(new SimpleGrantedAuthority(role));
        }
        return authorities;
    }

    private User createUser(Account account, List<GrantedAuthority> authorities) {
        return new User(
                account.getLogin(),
                account.getPassword(),
                authorities);
    }
}
