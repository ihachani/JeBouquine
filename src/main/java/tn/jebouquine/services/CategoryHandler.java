package tn.jebouquine.services;

import tn.jebouquine.pojo.Category;

public interface CategoryHandler {
    Category save(Category category);
}
