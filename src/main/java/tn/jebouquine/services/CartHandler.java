package tn.jebouquine.services;

import tn.jebouquine.pojo.Book;
import tn.jebouquine.pojo.Cart;

import java.math.BigDecimal;

public interface CartHandler {
    void addBook(Book book);

    void removeBook(Book book);

    //TODO might push this to pojo CART.
    BigDecimal getTotalPrice();

    BigDecimal getTotalPrice(Cart cart);
    Cart saveCart(Cart cart);
}
