package tn.jebouquine.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.jebouquine.dao.BookDAO;
import tn.jebouquine.dao.CartDAO;
import tn.jebouquine.pojo.Book;
import tn.jebouquine.pojo.Cart;

import java.math.BigDecimal;

@Service("cartHandler")
@Scope("prototype")
public class CartHandlerImpl implements CartHandler {

    @Autowired
    Cart cart;

    @Autowired
    BookDAO bookDAO;
    @Autowired
    CartDAO cartDAO;

    @Override
    public void addBook(Book book) {
        cart.addBook(book);
    }

    @Override
    public void removeBook(Book book) {
        cart.removeBook(book);
    }

    @Override
    public BigDecimal getTotalPrice() {
        return getTotalPrice(this.cart);
    }

    @Override
    public BigDecimal getTotalPrice(Cart cart) {

        return cart.getTotalPrice();
    }

    @Override
    @Transactional
    public Cart saveCart(Cart cart) {
        for (Book book : cart.getBooks()
                ) {
            bookDAO.update(book);
        }
        return cartDAO.create(cart);
    }
}
