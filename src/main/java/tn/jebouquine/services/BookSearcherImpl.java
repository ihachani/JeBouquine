package tn.jebouquine.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.jebouquine.dao.BookDAO;
import tn.jebouquine.pojo.Book;
import tn.jebouquine.pojo.Category;
import tn.jebouquine.pojo.forms.BookMultiCriteriaForm;

import java.util.List;

/**
 * Created by lonsomehell on 11/19/15.
 */
@Service("bookSearcher")
public class BookSearcherImpl implements BookSearcher {

    @Autowired
    BookDAO bookDAO;

    @Override
    public Book findByISBN(String isbn) {
        return bookDAO.retrieve(isbn);
    }

    @Override
    public List<Book> findByCategory(Category category) {
        return bookDAO.findByCategory(category);
    }

    @Override
    public List<Book> fastSearch(String searchTerm) {
        return bookDAO.fastSearch(searchTerm);
    }

    @Override
    public List<Book> multiCriteriaSearch(BookMultiCriteriaForm searchCriterias) {
        return bookDAO.multiCriteriaSearch(searchCriterias);
    }

    @Override
    public List<Book> getAll() {
        return bookDAO.findAll();
    }
}
