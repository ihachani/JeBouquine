package tn.jebouquine.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.jebouquine.dao.AuthorDAO;
import tn.jebouquine.pojo.Author;

@Service("authorHandler")
public class AuthorHandlerImpl implements AuthorHandler {
    @Autowired
    private AuthorDAO authorDAO;

    @Override
    @Transactional
    public Author save(Author author) {
        return authorDAO.create(author);
    }
}
