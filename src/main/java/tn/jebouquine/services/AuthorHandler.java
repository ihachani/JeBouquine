package tn.jebouquine.services;

import tn.jebouquine.pojo.Author;

public interface AuthorHandler {
    Author save(Author author);
}
