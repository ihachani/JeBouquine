package tn.jebouquine.services;

import tn.jebouquine.pojo.Account;
import tn.jebouquine.pojo.Cart;
import tn.jebouquine.pojo.Command;

import java.math.BigDecimal;
import java.util.List;

public interface CommandHandler {
    Command createCommand(Account account,Cart cart);
    Command saveCommand(Command command);
    BigDecimal getTotalPrice(Command command);
    List<Command> getUserCommands(Account user);
}
