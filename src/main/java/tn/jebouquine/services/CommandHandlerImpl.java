package tn.jebouquine.services;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.jebouquine.dao.CommandDAO;
import tn.jebouquine.pojo.Account;
import tn.jebouquine.pojo.Cart;
import tn.jebouquine.pojo.Command;

import java.math.BigDecimal;
import java.util.List;

@Service("commandHandler")
public class CommandHandlerImpl implements CommandHandler {
    @Autowired
    CommandDAO commandDAO;

    @Autowired
    CartHandler cartHandler;

    @Override
    public Command createCommand(Account account, Cart cart) {
        Command command = new Command();
        command.setAccount(account);
        command.setCart(cart);
        System.out.println(cart);
        System.out.println("CommandHandlerImpl.createCommand");
        System.out.println(command);
        command.setDate(new DateTime());//Sets the current time
        return command;
    }

    @Override
    @Transactional
    public Command saveCommand(Command command) {
//        command.setCart(cartHandler.saveCart(command.getCart()));
        return commandDAO.create(command);
    }

    @Override
    public BigDecimal getTotalPrice(Command command) {
        return cartHandler.getTotalPrice(command.getCart());
    }

    @Override
    public List<Command> getUserCommands(Account user) {
        return commandDAO.getUserCommands(user);
    }

}
