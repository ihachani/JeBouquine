package tn.jebouquine.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.jebouquine.dao.CategoryDAO;
import tn.jebouquine.pojo.Category;

@Service("categoryHandler")
public class CategoryHandlerImpl implements CategoryHandler {

    @Autowired
    private CategoryDAO categoryDAO;

    @Override
    @Transactional
    public Category save(Category category) {
        return categoryDAO.create(category);
    }
}
