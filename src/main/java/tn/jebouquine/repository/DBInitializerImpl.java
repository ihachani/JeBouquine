package tn.jebouquine.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import tn.jebouquine.Utilities.SampleDataCreator;
import tn.jebouquine.dao.AccountDAO;
import tn.jebouquine.dao.BookDAO;

@Repository("dbInitializer")
public class DBInitializerImpl implements DBInitializer {
    @Autowired
    private BookDAO bookDAO;
    @Autowired
    private AccountDAO accountDAO;
    @Autowired
    private SampleDataCreator sampleDataCreator;

    @Override
    @Transactional
    public void setupDB() {

        createBooks();
        createAccounts();
    }

    private void createBooks() {
        bookDAO.create(sampleDataCreator.getBook());
        bookDAO.create(sampleDataCreator.getBook2());
    }

    private void createAccounts() {

        accountDAO.create(sampleDataCreator.getAdminAccount());
        accountDAO.create(sampleDataCreator.getUserAccount());
    }


}
