package tn.jebouquine.repository;

public interface DBInitializer {
    void setupDB();
}
