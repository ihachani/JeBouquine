package tn.jebouquine.services;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import tn.jebouquine.dao.CategoryDAO;
import tn.jebouquine.pojo.Category;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CategoryHandlerImplTest {
    @Mock
    private CategoryDAO categoryDAO;
    @InjectMocks
    private CategoryHandlerImpl categoryHandler;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void saveTest() {
        CategoryCreator categoryCreator = new CategoryCreator().invoke();
        Category unsaved = categoryCreator.getUnsaved();
        Category saved = categoryCreator.getSaved();

        when(categoryHandler.save(unsaved)).thenReturn(saved);

        Category result = categoryHandler.save(unsaved);

        verify(categoryDAO, atLeastOnce()).create(unsaved);
        assertEquals(saved, result);
    }


    private class CategoryCreator {
        private Category unsaved;
        private Category saved;

        public Category getUnsaved() {
            return unsaved;
        }

        public Category getSaved() {
            return saved;
        }

        public CategoryCreator invoke() {
            unsaved = new Category();
            unsaved.setName("category");
            unsaved.setDescription("Description");
            saved = new Category();
            saved.setId(1l);
            saved.setName("category");
            saved.setDescription("Description");
            return this;
        }
    }
}
