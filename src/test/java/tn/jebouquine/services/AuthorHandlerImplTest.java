package tn.jebouquine.services;


import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import tn.jebouquine.dao.AuthorDAO;
import tn.jebouquine.pojo.Author;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class AuthorHandlerImplTest {

    @Mock
    private AuthorDAO authorDAO;
    @InjectMocks
    private AuthorHandlerImpl authorHandler;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void saveTest() {
        AuthorCreator authorCreator = new AuthorCreator().invoke();
        Author unsaved = authorCreator.getUnsaved();
        Author saved = authorCreator.getSaved();

        when(authorHandler.save(unsaved)).thenReturn(saved);

        Author result = authorHandler.save(unsaved);

        verify(authorDAO, atLeastOnce()).create(unsaved);
        assertEquals(saved, result);
    }

    private class AuthorCreator {
        private Author unsaved;
        private Author saved;

        public Author getUnsaved() {
            return unsaved;
        }

        public Author getSaved() {
            return saved;
        }

        public AuthorCreator invoke() {
            unsaved = new Author();
            unsaved.setName("author");
            saved = new Author();
            saved.setId(1l);
            saved.setName("author");
            return this;
        }
    }
}
