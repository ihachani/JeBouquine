package tn.jebouquine.services.security;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import tn.jebouquine.dao.AccountDAO;
import tn.jebouquine.pojo.Account;

import java.util.ArrayList;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Mockito.when;

public class AccountServiceTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @Mock
    private AccountDAO accountDAO;

    @InjectMocks
    private AccountService accountService;

    //    @Mock
    Account account;

    private static final String USERNAME = "DARTH VADER";

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        account = new Account();
        account.setLogin("DARTH VADER");
        account.setPassword("TOP SECRET");
    }

    @Test
    public void loadUserByUsernameTestNoRoles() throws Exception {

        when(accountDAO.retrieve(USERNAME)).thenReturn(account);
        UserDetails userDetails = accountService.loadUserByUsername(USERNAME);
        Assert.assertEquals(account.getLogin(), userDetails.getUsername());
        Assert.assertEquals(account.getPassword(), userDetails.getPassword());
//        Assert.assertThat(userDetails.getAuthorities(), hasItem(new SimpleGrantedAuthority("ROLE_USER")));
        Assert.assertEquals(0, userDetails.getAuthorities().size());
    }

    @Test
    public void loadUserByUsernameTestWithRoles() throws Exception {
        ArrayList<String> roles = getRoles();

        account.setRoles(roles);
        when(accountDAO.retrieve(USERNAME)).thenReturn(account);
        UserDetails userDetails = accountService.loadUserByUsername(USERNAME);
        Assert.assertEquals(account.getLogin(), userDetails.getUsername());
        Assert.assertEquals(account.getPassword(), userDetails.getPassword());
//        Assert.assertThat(userDetails.getAuthorities(), hasItem(new SimpleGrantedAuthority("ROLE_USER")));
        Assert.assertEquals(2, userDetails.getAuthorities().size());
    }

    private ArrayList<String> getRoles() {
        ArrayList<String> roles = new ArrayList<String>();
        roles.add("ROLE_USER");
        roles.add("ROLE_ADMIN");
        return roles;
    }

    @Test
    public void loadUserByUsernameTestNoUserExists() throws Exception {
        thrown.expect(UsernameNotFoundException.class);
        thrown.expectMessage(equalTo("User '" + USERNAME + "' not found."));

        when(accountDAO.retrieve(USERNAME)).thenReturn(null);
        UserDetails userDetails = accountService.loadUserByUsername(USERNAME);

    }

}
