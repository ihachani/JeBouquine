package tn.jebouquine.services;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import tn.jebouquine.Utilities.SampleDataCreator;
import tn.jebouquine.pojo.Cart;

import java.math.BigDecimal;

import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

public class CartHandlerImplTest {

    @Mock
    Cart cart;
    @InjectMocks
    CartHandlerImpl cartHandler;

    SampleDataCreator sampleDataCreator;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        sampleDataCreator = new SampleDataCreator();
    }

    @Test
    public void addBookTest() {
        cartHandler.addBook(sampleDataCreator.getBook());
        verify(cart, atLeastOnce()).addBook(sampleDataCreator.getBook());
    }

    @Test
    public void removeBookTest() {
        cartHandler.removeBook(sampleDataCreator.getBook());
        verify(cart, atLeastOnce()).removeBook(sampleDataCreator.getBook());
    }

    @Test
    public void testGetTotalPriceInternal() throws Exception {
        cartHandler.getTotalPrice();
        verify(cart, atLeastOnce()).getTotalPrice();
    }

    @Test
    public void testGetTotalPriceWithParam() throws Exception {
        Cart cart1 = sampleDataCreator.getCart();
        System.out.println(cart1);
        Assert.assertEquals(new BigDecimal("30.00"), cartHandler.getTotalPrice(cart1));
    }
}
