package tn.jebouquine.services;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import tn.jebouquine.dao.BookDAO;

import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

public class BookSearcherImplTest {
    @Mock
    BookDAO bookDAO;
    @InjectMocks
    BookSearcherImpl bookSearcher;

    private static final String ISBN = "an ISBN";

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findByISBNTest() {
        bookSearcher.findByISBN(ISBN);
        verify(bookDAO, atLeastOnce()).retrieve(ISBN);
    }
}
