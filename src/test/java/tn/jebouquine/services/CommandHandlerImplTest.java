package tn.jebouquine.services;

import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import tn.jebouquine.Utilities.SampleDataCreator;
import tn.jebouquine.config.SpringConfig;
import tn.jebouquine.dao.CommandDAO;
import tn.jebouquine.pojo.Account;
import tn.jebouquine.pojo.Cart;
import tn.jebouquine.pojo.Command;

import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

@ContextConfiguration(classes = {SpringConfig.class})
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class CommandHandlerImplTest {

    @Mock
    CommandDAO commandDAO;
    @Mock
    CartHandler cartHandler;
    @InjectMocks
    CommandHandlerImpl commandHandler;
    @Autowired
    SampleDataCreator sampleDataCreator;
    @Mock
    Account account;
    @Mock
    Cart cart;

    @Before
    public void setUp() throws Exception {
        DateTimeUtils.setCurrentMillisFixed(100000);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateCommand() throws Exception {
        DateTime aTimeToUseInThisTest = new DateTime();

        Command command = commandHandler.createCommand(account, cart);
        Assert.assertEquals(cart, command.getCart());
        Assert.assertEquals(account, command.getAccount());
        Assert.assertEquals(aTimeToUseInThisTest, command.getDate());
    }


    @Test
    public void testSaveCommand() throws Exception {
        Command command = new Command();
        commandHandler.saveCommand(command);
        verify(commandDAO, atLeastOnce()).create(command);
    }

    @Test
    public void testGetTotalPrice() throws Exception {
        Command command = sampleDataCreator.getCommand();
        System.out.println(commandHandler.getTotalPrice(command));
        verify(cartHandler,atLeastOnce()).getTotalPrice(command.getCart());
    }
}