package tn.jebouquine.utilities;

/**
 * Created by lonsomehell on 10/23/15.
 */

import org.junit.Test;
import tn.jebouquine.Utilities.Hash;

import static org.junit.Assert.assertEquals;

public class HashTest {
    static final String STRING_TO_HASH = "Lonedewler";
    static final String HASHED_STRING_VALUE = "ecb41bdfda1854c6642184e583bd5e9ff93585402bbfcbebb6a09a3dca7b233df97ff53" +
            "1016c96605a9c6e906c6feee1b7c5e7d4af58c95fac0472f9f935b189";

    @Test
    public void sha512Test() {
        assertEquals(HASHED_STRING_VALUE, Hash.sha512(STRING_TO_HASH));
    }
}
