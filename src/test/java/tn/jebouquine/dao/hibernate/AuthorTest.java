package tn.jebouquine.dao.hibernate;
import org.junit.*;
import tn.jebouquine.dao.AuthorDAO;
import tn.jebouquine.pojo.Author;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
/**
 * Created by lonsomehell on 10/23/15.
 */
public class AuthorTest {
    private Author author;
    private static AuthorDAO authorDAO;
    @BeforeClass
    public static void  beforeClass(){
        authorDAO = new HibernateAuthorDAO();
    }
    @Before
    public void setUp(){
        author = new Author();
        author.setName("Gerrard");
    }

    @Test
    public void createTest(){
        authorDAO.create(author);
    }

    @Test
    public void retrieveTest(){
        authorDAO.create(author);
        Author author1 = authorDAO.retrieve(author.getId());
        assertEquals(author,author1);
    }
}
