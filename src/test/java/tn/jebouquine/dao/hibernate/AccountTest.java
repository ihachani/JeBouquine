package tn.jebouquine.dao.hibernate;


import org.junit.*;
import tn.jebouquine.Utilities.Hash;
import tn.jebouquine.dao.AccountDAO;
import tn.jebouquine.pojo.Account;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by lonsomehell on 10/23/15.
 */
public class AccountTest {
    private Account account1;
    private Account account2;
    private static AccountDAO accountDAO;

    @BeforeClass
    public static void beforeClass() {
        accountDAO = new HibernateAccountDAO();
    }

    @AfterClass
    public static void afterClass() {
        accountDAO = null;
    }

    @Before
    public void setUp() {
        account1 = new Account();
        account1.setEmail("firstuser@jebouquine.tn");
        account1.setLogin("loner1");
        account1.setName("Lonesome");
        account1.setPassword(Hash.sha512("password"));
        account2 = new Account();
    }

    @After
    public void tearDown() {
        account2 = new Account();
    }

    @Test
    public void addAccount() {
        accountDAO.create(account1);
    }

    @Test
    public void retrieveAccount() {
        accountDAO.create(account1);
        account2 = accountDAO.retrieve("loner1");
        assertEquals(account1, account2);
    }

    @Test
    public void updateAccount() {
        account2.setEmail("firstuser@jebouquine.tn");
        account2.setLogin("loner2");
        account2.setName("Lonesome");
        account2.setPassword(Hash.sha512("password"));
        accountDAO.create(account2);

        account2.setEmail("firstuser@jebouquine.tn");
        account2.setLogin("loner2");
        account2.setName("Changed");
        account2.setPassword(Hash.sha512("password"));
        accountDAO.update(account2);
        Account account3 = accountDAO.retrieve("loner2");
        assertEquals(account2, account3);

    }

    @Test
    public void deleteAccount() {
        account2.setEmail("firstuser@jebouquine.tn");
        account2.setLogin("loner1");
        account2.setName("Changed");
        account2.setPassword(Hash.sha512("password"));
        accountDAO.delete(account2);
        Account result = accountDAO.retrieve("loner1");
        assertNull(result);
    }

}
