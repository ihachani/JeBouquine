package tn.jebouquine.dao.jpa;

import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import tn.jebouquine.config.SpringConfig;
import tn.jebouquine.dao.CartDAO;
import tn.jebouquine.pojo.*;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by lonsomehell on 11/19/15.
 */
@ContextConfiguration(classes = {SpringConfig.class})
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class JPACartTest {
    @Autowired
    private CartDAO cartDAO;

    private Cart cart;
    private Book book;
    private Book book1;

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {

    }

    @Before
    public void setUp() throws Exception {
        cart = new Cart();

        Author author = new Author();
        author.setName("The lonely developer");
        Author author1 = new Author();
        author1.setName("R.C.Martin");
        Category category = new Category();
        category.setName("IT");
        Category category1 = new Category();
        category1.setName("Software Engineering");


        BookComment bookComment = new BookComment();
        bookComment.setComment("Yahoo!!");
        BookComment bookComment1 = new BookComment();
        bookComment1.setComment("A great book");

        book = new Book();
        book.setISBN("isbn:97801370810785");
        book.setName("Book");
        book.setPrice(new BigDecimal("10.00"));
        book.getAuthors().add(author);
        book.getCategories().add(category1);
        book.getBookComments().add(bookComment);

        book1 = new Book();
        book1.setISBN("isbn:978013708108582");
        book1.setName("book1");
        book1.setPrice(new BigDecimal("10.00"));
        book1.getAuthors().add(author1);
        book1.getCategories().add(category1);
        book1.getBookComments().add(bookComment1);

        cart.getBooks().add(book);
        cart.getBooks().add(book1);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Transactional
    @Test
    public void testCreate() throws Exception {
        cartDAO.create(cart);
    }

    @Transactional
    @Test
    public void testRetrieve() throws Exception {
        cartDAO.create(cart);
        Cart cart1 = cartDAO.retrieve(cart.getId());
        assertEquals(cart, cart1);
    }

    @Transactional
    @Test
    public void testUpdate() throws Exception {
        cartDAO.create(cart);
        cart.getBooks().remove(book);

        Cart cart1 = new Cart();
        cart1.getBooks().add(book1);
        cart1.setId(cart.getId());

        assertEquals(cart, cart1);
    }

    @Transactional
    @Test
    public void testDelete() throws Exception {
        cartDAO.delete(cart);
        Cart cart1 = cartDAO.retrieve(cart.getId());
        assertNull(cart1);
    }
}
