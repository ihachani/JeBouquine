package tn.jebouquine.dao.jpa;

import org.hamcrest.collection.IsIterableContainingInOrder;
import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.Logger;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import tn.jebouquine.Utilities.SampleDataCreator;
import tn.jebouquine.config.JPAConfig;
import tn.jebouquine.config.SpringConfig;
import tn.jebouquine.dao.BookDAO;
import tn.jebouquine.pojo.Author;
import tn.jebouquine.pojo.Book;
import tn.jebouquine.pojo.BookComment;
import tn.jebouquine.pojo.Category;
import tn.jebouquine.pojo.forms.BookMultiCriteriaForm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by lonsomehell on 11/18/15.
 */
@ContextConfiguration(classes = {SpringConfig.class})
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class JPABookTest {
    private SampleDataCreator sampleDataCreator;
    private Book book;
    private Book book2;

    @Autowired
    private BookDAO jpaBookDAO;

    private static Logger logger;

    private Author author;
    private Author author1;

    private Category category;
    private Category category1;

    private BookComment bookComment;
    private BookComment bookComment1;

    @BeforeClass
    public static void beforeClass() {

        logger = LoggerFactory.logger(JPABookTest.class);
    }

    @AfterClass
    public static void afterClass() {
        logger = null;
    }

    @Before
    public void setUp() {
        sampleDataCreator = new SampleDataCreator();

        author = sampleDataCreator.getAuthor();
        author1 = sampleDataCreator.getAuthor1();

        category = sampleDataCreator.getCategory();
        category1 = sampleDataCreator.getCategory1();

        bookComment = sampleDataCreator.getBookComment();
        bookComment1 = sampleDataCreator.getBookComment1();

        book = sampleDataCreator.getBook();
        book2 = sampleDataCreator.getBook2();
    }

    @Test
    @Transactional
    public void createTest() {
        jpaBookDAO.deleteAll();
        jpaBookDAO.create(book);
    }

    @Test
    @Transactional
    public void retrieveTest() {
        book.setISBN("isbn:9780137081074");
        jpaBookDAO.create(book);
        Book book1 = jpaBookDAO.retrieve(book.getISBN());
        assertEquals(book, book1);
    }

    @Test
    @Transactional
    public void deleteTest() {
        book.setISBN("isbn:9780137081205");
        jpaBookDAO.delete(book);
        Book book1 = jpaBookDAO.retrieve(book.getISBN());
        assertNull(book1);
    }

    @Test
    @Transactional
    public void findAll() {

        setupListOfBooksInDatabase();

        List<Book> books = jpaBookDAO.findAll();

        List<Book> expectedBooks = createListWithBook1And2();

        Assert.assertThat(books,
                IsIterableContainingInOrder.contains(expectedBooks.toArray()));
    }

    private void setupListOfBooksInDatabase() {
//        jpaBookDAO.delete(book);
//        jpaBookDAO.delete(book2);

        jpaBookDAO.deleteAll();

        jpaBookDAO.create(book);
        jpaBookDAO.create(book2);
    }

    private List<Book> createListWithBook1And2() {
        List<Book> expectedBooks = new ArrayList<Book>();
        expectedBooks.add(book);
        expectedBooks.add(book2);
        return expectedBooks;
    }

    @Test
    @Transactional
    public void findByCategory() {
        Book book3 = new Book();
        book3.setISBN("isbn:97801370810735");
        book3.setName("The Adventures of the little guy in Java persistence world");
        book3.setPrice(new BigDecimal("10.00"));
        book3.getAuthors().add(author);
        book3.getAuthors().add(author1);
        book3.getCategories().add(category);
        book3.getCategories().add(category1);
        book3.getBookComments().add(bookComment);
        book3.getBookComments().add(bookComment1);


        setupListOfBooksInDatabase();
        jpaBookDAO.create(book3);

        List<Book> books = jpaBookDAO.findByCategory(category1);

        List<Book> expectedBooks = new ArrayList<Book>();


        expectedBooks.add(book);
        expectedBooks.add(book3);

        Assert.assertThat(books,
                IsIterableContainingInOrder.contains(expectedBooks.toArray()));

    }

    @Test
    @Transactional
    public void fastSearchBothBooks() {
        setupListOfBooksInDatabase();

        List<Book> books = jpaBookDAO.fastSearch("BoOk");

        List<Book> expectedBooks = createListWithBook1And2();

        Assert.assertThat(books,
                IsIterableContainingInOrder.contains(expectedBooks.toArray()));
    }

    @Test
    @Transactional
    public void fastSearchNoBooks() {
        setupListOfBooksInDatabase();

        List<Book> books = jpaBookDAO.fastSearch("fazeasf");
        assertTrue(books.isEmpty());
    }

    @Test
    @Transactional
    public void fastSearchOneHitISBN() {
        setupListOfBooksInDatabase();

        List<Book> books = jpaBookDAO.fastSearch("9780137081073");

        List<Book> expectedBooks = new ArrayList<Book>();
        expectedBooks.add(book2);

        Assert.assertThat(books,
                IsIterableContainingInOrder.contains(expectedBooks.toArray()));
    }

    @Test
    @Transactional
    public void fastSearchOneHitName() {
        setupListOfBooksInDatabase();

        List<Book> books = jpaBookDAO.fastSearch("Book2");

        List<Book> expectedBooks = new ArrayList<Book>();
        expectedBooks.add(book2);

        Assert.assertThat(books,
                IsIterableContainingInOrder.contains(expectedBooks.toArray()));
    }

    @Test
    @Transactional
    public void multipleCriterias() {
        setupListOfBooksInDatabase();

        BookMultiCriteriaForm searchCriterias = new BookMultiCriteriaForm();
        searchCriterias.setBookName("Book2");
        searchCriterias.setISBN("isbn:9780137081073");

        List<Book> books = jpaBookDAO.multiCriteriaSearch(searchCriterias);

        List<Book> expectedBooks = new ArrayList<Book>();
        expectedBooks.add(book2);

        assertEquals(expectedBooks, books);

    }
}

