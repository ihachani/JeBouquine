package tn.jebouquine.dao.jpa;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import tn.jebouquine.Utilities.SampleDataCreator;
import tn.jebouquine.config.SpringConfig;
import tn.jebouquine.dao.AccountDAO;
import tn.jebouquine.dao.CommandDAO;
import tn.jebouquine.pojo.Command;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@ContextConfiguration(classes = {SpringConfig.class})
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class JPACommandDAOTest {

//    @Autowired
//    private CommandDAO commandDAO;
//    @Autowired
//    private SampleDataCreator sampleDataCreator;
//    @Autowired
//    private AccountDAO accountDAO;
//
//    @Test
//    @Transactional
//    public void testCreate() throws Exception {
//        accountDAO.update(sampleDataCreator.getCommand().getAccount());
//        Command result = commandDAO.create(sampleDataCreator.getCommand());
//        assertNotNull(result);
//    }
//
//    @Test
//    @Transactional
//    public void testGetUserCommands() throws Exception {
//        accountDAO.update(sampleDataCreator.getCommand().getAccount());
//        accountDAO.update(sampleDataCreator.getCommand2().getAccount());
//        Command result = commandDAO.create(sampleDataCreator.getCommand());
//        commandDAO.create(sampleDataCreator.getCommand1());
//        commandDAO.create(sampleDataCreator.getCommand2());
//        List<Command> commands = commandDAO.getUserCommands(sampleDataCreator.getCommand().getAccount());
//        assertEquals(2, commands.size());
//    }

}