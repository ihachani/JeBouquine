package tn.jebouquine.pojo;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

/**
 * Created by lonsomehell on 10/24/15.
 */
public class BookTest {
    Author author;
    BookComment bookComment;
    Category category;
    @Before
    public void setUp() {

        author = new Author();
        author.setId(12);
        author.setName("aze");

        bookComment = new BookComment();
        bookComment.setComment("Yeppi");

        category = new Category();
        category.setId(1);
        category.setName("Hope");
        category.setDescription("This is just a category");
    }

    @Test
    public void equalsWithMininmumFields() {
        Book book = new Book();
        book.setISBN("A");
        book.setName("Name");
        book.setPrice(new BigDecimal("10.0"));
        Book book1 = new Book();
        book1.setISBN("A");
        book1.setName("Name");
        book1.setPrice(new BigDecimal("10.0"));
        assertEquals(book, book1);
    }

    @Test
    public void equalsWithAuthorCollection() {


        Book book = new Book();
        book.setISBN("A");
        book.setName("Name");
        book.setPrice(new BigDecimal("10.0"));
        book.getAuthors().add(author);

        Book book1 = new Book();
        book1.setISBN("A");
        book1.setName("Name");
        book1.setPrice(new BigDecimal("10.0"));
        book1.getAuthors().add(author);

        assertEquals(book, book1);
    }

    @Test
    public void equalsWithBookCommentsCollection() {


        Book book = new Book();
        book.setISBN("A");
        book.setName("Name");
        book.setPrice(new BigDecimal("10.0"));
        book.getCategories().add(category);

        Book book1 = new Book();
        book1.setISBN("A");
        book1.setName("Name");
        book1.setPrice(new BigDecimal("10.0"));
        book1.getCategories().add(category);

        assertEquals(book, book1);
    }

    @Test
    public void equalsWithCategoriesCollection() {


        Book book = new Book();
        book.setISBN("A");
        book.setName("Name");
        book.setPrice(new BigDecimal("10.0"));
        book.getCategories().add(category);



        Book book1 = new Book();
        book1.setISBN("A");
        book1.setName("Name");
        book1.setPrice(new BigDecimal("10.0"));
        book1.getCategories().add(category);

        assertEquals(book, book1);
    }

    @Test
    public void equalsWithComplexFullBook() {


        Book book = new Book();
        book.setISBN("A");
        book.setName("Name");
        book.setPrice(new BigDecimal("10.0"));
        book.setDescription("A really great book");
        book.getBookComments().add(bookComment);
        book.getCategories().add(category);
        book.getAuthors().add(author);


        Book book1 = new Book();
        book1.setISBN("A");
        book1.setName("Name");
        book1.setPrice(new BigDecimal("10.0"));
        book1.setDescription("A really great book");
        book1.getBookComments().add(bookComment);
        book1.getCategories().add(category);
        book1.getAuthors().add(author);

        assertEquals(book, book1);
    }
}
