package tn.jebouquine.controllers;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.view.InternalResourceView;
import tn.jebouquine.pojo.Book;
import tn.jebouquine.pojo.Cart;
import tn.jebouquine.services.BookSearcher;
import tn.jebouquine.services.CartHandler;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

public class CartControllerTest {
    @Mock
    CartHandler cartHandler;
    @Mock
    BookSearcher bookSearcher;
    @Mock
    Cart cart;
    @InjectMocks
    CartController controller;
    @Mock
    Book book;
    private static final String ADD_URL = "/cart/add";
    private static final String REMOVE_URL = "/cart/remove";
    private static final String CART_URL = "/cart";
    private static final String CART_CONTENT_VIEW_NAME = "booksInCart";
    private static final String ISBN_KEY = "isbn";
    final String dummyIsbnValue = "123456";

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void handleAddBookToCartTest() throws Exception {
        when(bookSearcher.findByISBN(dummyIsbnValue)).thenReturn(book);
        MockMvc mockMvc = standaloneSetup(controller)
//                .setSingleView(
//                        new InternalResourceView(VIEW_URL))
                .build();


        mockMvc.perform(post(ADD_URL).param(ISBN_KEY, dummyIsbnValue));
//                .andExpect(
//                        view().name(CART_CONTENT_VIEW_NAME));
        verify(bookSearcher, atLeastOnce()).findByISBN(dummyIsbnValue);
        verify(cartHandler, atLeastOnce()).addBook(book);
    }

    @Test
    public void showBooksInCartTest() throws Exception {
//        when(bookSearcher.findByISBN(dummyIsbnValue)).thenReturn(book);
        MockMvc mockMvc = standaloneSetup(controller)
                .setSingleView(
                        new InternalResourceView(CART_CONTENT_VIEW_NAME))
                .build();


        mockMvc.perform(get(CART_URL))
                .andExpect(
                        view().name(CART_CONTENT_VIEW_NAME))
                .andExpect(model().attributeExists("booksInCart"))
        ;
//        verify(bookSearcher, atLeastOnce()).findByISBN(dummyIsbnValue);
//        verify(cartHandler, atLeastOnce()).addBook(book);
    }

    @Test
    public void handleRemoveBookFromCartTest() throws Exception {
        when(bookSearcher.findByISBN(dummyIsbnValue)).thenReturn(book);
        MockMvc mockMvc = standaloneSetup(controller)
//                .setSingleView(
//                        new InternalResourceView(VIEW_URL))
                .build();


        mockMvc.perform(post(REMOVE_URL).param(ISBN_KEY, dummyIsbnValue));
//                .andExpect(
//                        view().name(CART_CONTENT_VIEW_NAME));
        verify(bookSearcher, atLeastOnce()).findByISBN(dummyIsbnValue);
        verify(cartHandler, atLeastOnce()).removeBook(book);
    }
}
