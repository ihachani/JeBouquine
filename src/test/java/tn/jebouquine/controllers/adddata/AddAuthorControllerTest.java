package tn.jebouquine.controllers.adddata;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.view.InternalResourceView;
import tn.jebouquine.pojo.Author;
import tn.jebouquine.services.AuthorHandler;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;


public class AddAuthorControllerTest {
    private final String FORM_VIEW_URL = "/WEB-INF/templates/addauthor.html";
    private final String FORM_URL = "/addAuthor";
    private final String FORM_VIEW_NAME = "addAuthor";
    private final String REDIRECTED_URL = "/index.html";
    @Mock
    private AuthorHandler authorHandler;
    @InjectMocks
    private AddAuthorController controller = new AddAuthorController();

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void showAddAuthorFormTest() throws Exception {
        AddAuthorController controller = new AddAuthorController();

        MockMvc mockMvc = standaloneSetup(controller).
                setSingleView(
                        new InternalResourceView(FORM_VIEW_URL))
                .build();


        mockMvc.perform(get(FORM_URL))
                .andExpect(
                        view().name(FORM_VIEW_NAME));
    }

    @Test
    public void handleAddAuthorTest() throws Exception {

        Author unsaved = new Author();
        unsaved.setName("author");
        Author saved = new Author();
        saved.setId(1l);
        saved.setName("author");
        when(authorHandler.save(unsaved)).thenReturn(saved);


        MockMvc mockMvc = standaloneSetup(controller).
                setSingleView(
                        new InternalResourceView(FORM_VIEW_URL))
                .build();


        mockMvc.perform(post(FORM_URL)
                .param("name", "author"))
                .andExpect(status().isOk());
//        .andExpect(redirectedUrl(REDIRECTED_URL)
//        For Some reason it always returns null but the view returns index.html
        verify(authorHandler, atLeastOnce()).save(unsaved);

    }
}
