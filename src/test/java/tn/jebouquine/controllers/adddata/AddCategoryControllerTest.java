package tn.jebouquine.controllers.adddata;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.view.InternalResourceView;
import tn.jebouquine.pojo.Category;
import tn.jebouquine.services.CategoryHandler;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

public class AddCategoryControllerTest {
    private final String FORM_VIEW_URL = "/WEB-INF/templates/addCategory.html";
    private final String FORM_URL = "/addCategory";
    private final String FORM_VIEW_NAME = "addCategory";
    private final String REDIRECTED_URL = "/index.html";

    @Mock
    CategoryHandler categoryHandler;
    @InjectMocks
    AddCategoryController controller;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void showAddAuthorFormTest() throws Exception {

        AddCategoryController controller = new AddCategoryController();

        MockMvc mockMvc = standaloneSetup(controller).
                setSingleView(
                        new InternalResourceView(FORM_VIEW_URL))
                .build();


        mockMvc.perform(get(FORM_URL))
                .andExpect(
                        view().name(FORM_VIEW_NAME));
    }

    @Test
    public void handleAddAuthorTest() throws Exception {

        Category unsaved = new Category();
        unsaved.setName("category");
        unsaved.setDescription("Description");
        Category saved = new Category();
        saved.setId(1l);
        saved.setName("category");
        saved.setDescription("Description");
        when(categoryHandler.save(unsaved)).thenReturn(saved);


        MockMvc mockMvc = standaloneSetup(controller).
                setSingleView(
                        new InternalResourceView(FORM_VIEW_URL))
                .build();


        mockMvc.perform(post(FORM_URL)
                .param("name", "category")
                .param("description", "Description"))
                .andExpect(status().isOk());
//        .andExpect(redirectedUrl(REDIRECTED_URL)
//        For Some reason it always returns null but the view returns index.html
        verify(categoryHandler, atLeastOnce()).save(unsaved);

    }
}
