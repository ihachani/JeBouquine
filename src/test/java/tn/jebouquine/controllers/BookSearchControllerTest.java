package tn.jebouquine.controllers;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.view.InternalResourceView;
import tn.jebouquine.pojo.Book;
import tn.jebouquine.pojo.forms.BookMultiCriteriaForm;
import tn.jebouquine.services.BookSearcher;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

public class BookSearchControllerTest {
    @Mock
    BookSearcher bookSearcher;
    @InjectMocks
    BookSearchController controller;
    @Mock
    Book book;
    @Mock
    Book book2;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
//        sampleDataCreator = new SampleDataCreator();
    }

    @Test
    public void showSearchPageTest() throws Exception {

        MockMvc mockMvc = standaloneSetup(controller).
                setSingleView(
                        new InternalResourceView("/WEB-INF/views/searchTest.jsp"))
                .build();
        mockMvc.perform(get("/searchTest"))
                .andExpect(
                        view().name("searchTest"))
                .andExpect(
                        model().attributeExists("multiCriteriaForm")
                );

    }

    @Test
    public void showResultsTest() throws Exception {
        BookMultiCriteriaForm searchCriteria = new BookMultiCriteriaForm();
        List<Book> books = new ArrayList<Book>();
        List<Book> expected = new ArrayList<Book>();

        expected.add(book);
        expected.add(book2);
        books.add(book);
        books.add(book2);

        when(bookSearcher.multiCriteriaSearch(searchCriteria)).thenReturn(books);
        System.out.println(bookSearcher.multiCriteriaSearch(new BookMultiCriteriaForm()));
        MockMvc mockMvc = standaloneSetup(controller).
                setSingleView(
                        new InternalResourceView("/WEB-INF/views/viewBooks.jsp"))
                .build();
        mockMvc.perform(get("/searchResult"))
                .andExpect(
                        view().name("viewBooks"))
                .andExpect(
                        model().attributeExists("multiCriteriaForm"))
                .andExpect(
                        model().attributeExists("books"))
                .andExpect(
                        model().attribute("books", hasItems(expected.toArray())));
    }

}
